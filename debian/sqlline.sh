#!/bin/sh

# Include the wrappers utility script
. /usr/lib/java-wrappers/java-wrappers.sh

# We need a java runtime (any 1.4 work)
find_java_runtime all

# Define our classpath
find_jars jline sqlline postgresql-jdbc4 mariadb-java-client hsqldb jtds

# Run SQLLine
run_java sqlline.SqlLine $extra_args "$@"
